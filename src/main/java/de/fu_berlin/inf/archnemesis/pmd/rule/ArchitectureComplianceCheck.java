package de.fu_berlin.inf.archnemesis.pmd.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTImportDeclaration;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
import de.fu_berlin.inf.archnemesis.archnemesis.Architecture;
import de.fu_berlin.inf.archnemesis.archnemesis.Component;
import de.fu_berlin.inf.archnemesis.archnemesis.Constraint;
import de.fu_berlin.inf.archnemesis.core.chralx.ChralxSingleton;

public class ArchitectureComplianceCheck extends AbstractJavaRule {
    private Architecture arc;
    private Map<Component, Set<Component>> componentToAccess;
    private List<Constraint> constraintList;

    public ArchitectureComplianceCheck() {
        super();
    }

    /**
     * Runs architecture checks on CompilationUnits (Source file)
     *
     * @param node
     *            The current compilation unit node in the AST
     * @param data
     *            Contains info on violations for each node of the tree scanned.
     */
    @Override
    public Object visit(ASTCompilationUnit node, Object data) {
        arc = ChralxSingleton.getInstance().getArc();
        componentToAccess = ChralxSingleton.getInstance()
                .getComponentToAccess();
        constraintList = ChralxSingleton.getInstance().getConstraints();

        // AST Analysis
        List<ASTImportDeclaration> importDecList = node
                .findChildrenOfType(ASTImportDeclaration.class);
        Set<Component> componentSet = componentToAccess.keySet();

        // check constraints
        for (Constraint c : constraintList) {
            applyConstraintCheck(node, importDecList, c, data);
        }

        // check component access
        for (Component component : componentSet) {
            if (isInComponent(node, component)) {
                applyLayerAccessCheck(importDecList, componentSet, component,
                        node, data);
            }
        }
        return super.visit(node, data);
    }
    
    public void setChralxLocation(String path) {
        ChralxSingleton.getInstance(path);
    }

    /**
     * Checks the code for constraint violations. If an import that should be
     * there is missing it will add a violation, warning the user that there is
     * a possible error
     */
    private void applyConstraintCheck(ASTCompilationUnit node,
            List<ASTImportDeclaration> importDecList, Constraint c, Object data) {
        for (ASTImportDeclaration i : importDecList) {
            if (isInComponent(i, c.getComponent())) {
                if (!isInImports(c.getRequiredPackage(), importDecList)) {
                    addViolationWithMessage(
                            data,
                            node,
                            "The component "
                                    + c.getComponent().getName()
                                    + " should generally be used together with "
                                    + c.getRequiredPackage()
                                    + ". Are you sure this code is not faulty?");
                    break;
                }
            }
        }
    }

    /**
     * Checks for invalid imports. If it finds an import that is listed in a
     * component's namespace list but should not be accessible then it will log
     * an error
     */
    private void applyLayerAccessCheck(
            List<ASTImportDeclaration> importDecList,
            Set<Component> componentSet, Component component,
            ASTCompilationUnit node, Object data) {
        Set<Component> accessibleComponents = componentToAccess.get(component);
        for (int i = 0; i < importDecList.size(); i++) {
            ASTImportDeclaration currentImport = importDecList.get(i);
            if (isInComponentSet(currentImport, componentSet)) {
                if (!isInComponentSet(currentImport, accessibleComponents)) {
                    addViolationWithMessage(data, node,
                            "Please refer to the architecture DSL arhitecture.chralx under SAROS_ROOT.");
                }
            }
        }
    }

    private boolean isInComponent(ASTCompilationUnit node, Component component) {
        String packageName = node.getPackageDeclaration().getPackageNameImage();
        return isInNamespaceList(packageName, component.getNamespaces());
    }

    private boolean isInComponent(ASTImportDeclaration i, Component component) {
        String importName = i.getImportedName();

        return isInNamespaceList(importName, component.getNamespaces());
    }

    private boolean isInComponentSet(ASTImportDeclaration currentImport,
            Set<Component> componentSet) {
        boolean result = false;
        for (Component c : componentSet) {
            result |= isInComponent(currentImport, c);
        }
        return result;
    }

    private boolean isInNamespaceList(String importName,
            List<String> namespaceList) {
        boolean isIn = false;
        for (String namespace : namespaceList) {
            isIn |= importName.matches(qualifiedNameToRegex(namespace));
        }
        return isIn;
    }

    private boolean isInImports(String requiredPackage,
            List<ASTImportDeclaration> importDecs) {
        return isInNamespaceList(requiredPackage, toStringList(importDecs));
    }

    private List<String> toStringList(List<ASTImportDeclaration> imports) {
        List<String> list = new ArrayList<String>();
        for (ASTImportDeclaration i : imports) {
            list.add(i.getPackageName());
        }
        return list;
    }

    private String qualifiedNameToRegex(String qualifiedName) {
        qualifiedName = qualifiedName.replaceAll("\\.", "\\\\.");
        return qualifiedName = qualifiedName + ".*";
    }
}
