package de.fu_berlin.inf.archnemesis.pmd.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import net.sourceforge.pmd.PMD;
import net.sourceforge.pmd.PMDException;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.RuleSet;
import net.sourceforge.pmd.RuleSets;
import net.sourceforge.pmd.SourceCodeProcessor;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fu_berlin.inf.archnemesis.pmd.rule.ArchitectureComplianceCheck;

public class ArchitectureRuleTest {

    private static PMD instance;
    private static SourceCodeProcessor pro;
    private static InputStream logicStream;
    private static InputStream networkStream;
    private static RuleSets ruleset;

    @BeforeClass
    public static void setup() {
        // New PMD instance
        instance = new PMD();
        // get sourceCodeProcessor
        pro = instance.getSourceCodeProcessor();
        // Make file input streams
        try {
            networkStream = new FileInputStream(new File(
                    "src/test/resources/packages/bar/network/network.java"));
            logicStream = new FileInputStream(new File(
                    "src/test/resources/packages/bar/logic/logic.java"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Setup testRule to use the chralx file in the res folder
        ArchitectureComplianceCheck testRule = new ArchitectureComplianceCheck();
        testRule.setChralxLocation("res/testArch.chralx");
        // Make Ruleset containing ArchitectureComplianceCheck rule
        RuleSet rule = new RuleSet();
        rule.addRule(testRule);
        ruleset = new RuleSets();
        ruleset.addRuleSet(rule);
    }

    @Test
    public void testComponentAccessWithError() {
        RuleContext ctx = PMD.newRuleContext("network.java", new File(
                "src/test/packages/bar/network/network.java"));
        try {
            pro.processSourceCode(networkStream, ruleset, ctx);
        } catch (PMDException e) {
            e.printStackTrace();
        }
        Assert.assertFalse(ctx.getReport().isEmpty());
    }

    @Test
    public void testComponentAccessNoError() {
        RuleContext ctx = PMD.newRuleContext("logic.java", new File(
                "src/test/packages/bar/logic/logic.java"));
        try {
            pro.processSourceCode(logicStream, ruleset, ctx);
        } catch (PMDException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(ctx.getReport().isEmpty());
    }
}