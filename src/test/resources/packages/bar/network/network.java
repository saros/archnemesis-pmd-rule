package packages.bar.network;

import packages.bar.logic.logic;

// Should cause error, network cannot depend on logic
public class network {

    public static void networkFunction() {
        System.out.println("Network function");
    }

    public static void networkAndLogic() {
        System.out.print("network and ");
        logic.logicFunction();
    }
}
