package packages.bar.logic;

import packages.bar.network.network;

// Import causes no error, logic depends on network
public class logic {

    public static void logicFunction() {
        System.out.println("logic");
    }

    public static void logicAndNetwork() {
        System.out.print("logic and ");
        network.networkFunction();
    }
}
